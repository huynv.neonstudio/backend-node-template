const express = require('express');
const app = express()
const router = require('./routers')
const cors = require('cors');
app.use(cors({credentials: false,origin: '*'}));
const PORT =  process.env.PORT || 3001
app.use(express.static('public'))
app.use('/assets', express.static(__dirname + '/assets'));
app.use(express.json());

app.use(express.urlencoded({
  extended: true
}));
app.use(router)
app.listen(PORT,()=>{
    console.log("server running port ", PORT)
})
