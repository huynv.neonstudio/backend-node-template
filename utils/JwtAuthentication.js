var path = require('path')
var fs = require("fs")
var jwt = require('jsonwebtoken')
const mongoDB = require('../db/mongoDB')
const responseStatus = require('../constants/responseStatus')
const rsaCertificationAbsolutePath = path.resolve(__dirname + "./../jwt_key/jwtRS256.key.pub")

class JwtAuthentication {
    constructor() {
        this.rsaCertification = fs.readFileSync(rsaCertificationAbsolutePath);
    }
    async verifyToken(token) {
        let verifiedData = await jwt.verify(token, this.rsaCertification, { ignoreExpiration: false }, (err, decoded) => {
            if (err) {
                return { isSuccess: false, err: err }
            } else {
                return { isSuccess: true, err: null }
            }
        });
        if (verifiedData.isSuccess) {
            return true
        } else {
            return false
        }
    }
    userTokenAuthentication() {
        return async (req, res, next) => {
            req.isRequireCheckToken = true
            if (req.isRequireCheckToken) {
                let userToken = req.headers.token
                if (userToken) {
                    let userInfo = await mongoDB.users.findUserByToken(userToken)
                    if (!userInfo.isSuccess) return res.json(responseStatus.UNAUTHORIZED)
                    if (!req.body.userId)
                        req.body.userId = userInfo.data._id.toString()
                    if(req.body.userId) req.body.userAuthId = userInfo.data._id.toString()
                    let verifiedData = await jwt.verify(userToken, this.rsaCertification, { ignoreExpiration: false }, (err, decoded) => {
                        if (err) {
                            return { isSuccess: false, err: err }
                        } else {
                            return { isSuccess: true, err: null }
                        }
                    });
                    if (verifiedData.isSuccess) {
                        req.isUserIsAuthenticated = true
                        await next()
                    } else {
                        req.isUserIsAuthenticated = false
                        return res.json(responseStatus.UNAUTHORIZED)
                    }
                } else {

                    return res.json(responseStatus.UNAUTHORIZED)
                }
            } else {
                await next()
            }
        }
    }
}
module.exports = new JwtAuthentication()