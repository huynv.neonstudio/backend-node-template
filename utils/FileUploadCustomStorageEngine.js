const constants = require('../constants')
// const mime = require('mime-types')
// var Jimp = require('jimp');
const path = require('path')
var fs = require('fs')
// const sharp = require('sharp');
const moment = require('moment')
async function getDestination(req, file, cb) {
    var ext = path.extname(file.originalname)
    var newFileName = randomFileID()
    // var currentFileName = Date.now() + newFileName + '.' + mime.extension(file.mimetype)
    var currentFileName = Date.now() + newFileName + ext
    let dateTimeString = moment().format("DD-MM-YYYY") + "/";
    let createDir = await makeDir(constants.consts.SERVER_LOCATION_FOR_STORAGE + dateTimeString)
    let originalPath = path.join(constants.consts.SERVER_LOCATION_FOR_STORAGE + dateTimeString)
    cb(null, originalPath + currentFileName)
}
function randomFileID() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 12; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function MyCustomStorage(opts) {
    this.getDestination = (opts.destination || getDestination)
}
async function makeDir(dirpath) {
    try {
        return fs.mkdirSync(dirpath)
    } catch (err) {
        if (err.code !== 'EEXIST') throw err
    }
}
function makeThumbnail(filePath) {
    return new Promise(async (resolve, reject) => {
        let dateTimeString = moment().format("DD-MM-YYYY") + "/";
        let createDir = await makeDir(constants.consts.SERVER_LOCATION_FOR_STORAGE_THUMBNAIL + dateTimeString)
        let thumbnailPath = path.join(constants.consts.SERVER_LOCATION_FOR_STORAGE_THUMBNAIL + dateTimeString + "./thumnail_" + path.basename(filePath))
        sharp(path.join("./" + filePath))
        .resize(256, 256)
        .png({quality:50})
        .toFile(thumbnailPath,
         (err, info) => { 
             if(err)
             {
            console.log(err)
             }else {
                resolve({
                    thumbnailPath: thumbnailPath
                })
             }
          });   
            // .then(lenna => {
            //     return lenna
            //         .scaleToFit(256, 256) // resize
            //         .quality(60) // set JPEG quality
            //         .write(thumbnailPath); // save
            // })
            // .catch(err => {
            //     console.error(err);
            // }).then(result => {
            //     resolve({
            //         thumbnailPath: thumbnailPath
            //     })
            // });
    })
}
MyCustomStorage.prototype._handleFile = function _handleFile(req, file, cb) {
    this.getDestination(req, file, function (err, path) {
        if (err) return cb(err)
        var outStream = fs.createWriteStream(path)
        file.stream.pipe(outStream)
        outStream.on('error', cb)
        outStream.on('finish', function (data) {
            // makeThumbnail(path).then(thumbnailPath => {
                cb(null, {
                    path: path,
                    size: outStream.bytesWritten,
                    originalname: file.originalname,
                    // thumbnailPath: thumbnailPath.thumbnailPath
                // })
            })

        })
    })
}

MyCustomStorage.prototype._removeFile = function _removeFile(req, file, cb) {
    fs.unlink(file.path, cb)
}

module.exports = function (opts) {
    return new MyCustomStorage(opts)
}