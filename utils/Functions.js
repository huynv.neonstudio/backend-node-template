
const constants = require('./../constants')
const moment = require('moment')
const ObjectId =require('mongodb').ObjectID
const mime = require('mime-types')
// var Jimp = require('jimp');
const path = require('path')
var fs = require('fs')

class Functions { 
    constructor(){
    }
     getDetailImageFromUrl(pathOriginal = "", pathThumnail = ""){
        let imageName = pathOriginal.split('/').pop()
        var fileURL = pathOriginal.replace(/\\/g, "/");
        fileURL = fileURL.replace(`${constants.consts.SERVER_LOCATION_FOR_STORAGE.replace("./","")}`, `${constants.consts.URL_GENERATE_FOR_FILE_SHARE}`)
        var filePath = pathOriginal
        // var thumbnailUrl = pathThumnail.replace(/\\/g, "/");
        // thumbnailUrl = thumbnailUrl.replace(`${constants.SERVER_LOCATION_FOR_STORAGE_THUMBNAIL.replace("./","")}`, `${constants.URL_GENERATE_FOR_FILE_SHARE_THUMBNAIL}`)
        return new Object({
            imageName: imageName,
            originalPath: pathOriginal,
            // thumbnailPath : pathThumnail,
            fileURL : fileURL,
            filePath : filePath
            // thumbnailUrl: thumbnailUrl,
        })
    }
    removeAllSpace(str)
    {
        return str.replace(/\s/g, "")
    }
    xoa_dau(str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");
        return str;
    }
    async  makeDir(dirpath) {
        try {
            return fs.mkdirSync(dirpath)
        } catch (err) {
            if (err.code !== 'EEXIST') throw err
        }
    }
     makeThumbnail(filePath) {
        return new Promise(async (resolve, reject) => {
            let dateTimeString = moment().format("DD-MM-YYYY") + "/";
            let createDir = await  this.makeDir(constants.SERVER_LOCATION_FOR_STORAGE_THUMBNAIL + dateTimeString)
            let thumbnailPath = path.join(constants.SERVER_LOCATION_FOR_STORAGE_THUMBNAIL + dateTimeString + "./thumnail_" + path.basename(filePath))
            Jimp.read(path.join("./" + filePath))
                .then(lenna => {
                    return lenna
                        .scaleToFit(256, 256) // resize
                        .quality(60) // set JPEG quality
                        .write(thumbnailPath); // save
                })
                .catch(err => {
                    console.error(err);
                }).then(result => {
                    resolve({
                        thumbnailPath: thumbnailPath
                    })
                });
        })
    }
    randomFileID() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
        for (var i = 0; i < 12; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
    
        return text;
    }
    updateCurrentRankingTags(upvote, downvote = 0, timeLastUpdate){
        let moment_now = moment();
        let expire_time = moment(timeLastUpdate);
        let duration = moment.duration(moment_now.diff(expire_time));
        let duraTime = duration/1000/60/60
        let score = (upvote - downvote) - duraTime/constants.TIME_ELASPED
        return score
    }
    objectIdWithTimestamp(timestamp) {
        // Convert string date to Date object (otherwise assume timestamp is a date)
        if (typeof(timestamp) == 'string') {
            timestamp = new Date(timestamp);
        }
    
        // Convert date object to hex seconds since Unix epoch
        var hexSeconds = Math.floor(timestamp/1000).toString(16);
    
        // Create an ObjectId with that hex timestamp
        var constructedObjectId = ObjectId(hexSeconds + "0000000000000000");
    
        return constructedObjectId
    }
    
}
module.exports = new Functions()