const responseStatus = require("../constants/responseStatus")
const permissionResource = require("../db/mongoDB/permissionResource")
const roles = require("../db/mongoDB/roles")
const users = require("../db/mongoDB/users")

module.exports = {
    checkAdmin: async (req, res, next) => {
        let userToken = req.headers.token
        let userInfo = await users.findUserByToken(userToken)
        if (userInfo.isSuccess) {
            const roleInfo = await roles.findById(userInfo.data.roleId)
            if (roleInfo.isSuccess) {
                if (roleInfo.data.roleName == "admin")
                    await next()
                else {
                    res.json(responseStatus.BAD_REQUEST)
                }
            }else
            return res.json(responseStatus.BAD_REQUEST)
        }else
        return res.json(responseStatus.BAD_REQUEST)
    },
    checkUser: async (req, res, next) => {
        let userToken = req.headers.token
        let userInfo = await users.findUserByToken(userToken)
        if (userInfo.isSuccess) {
            const roleInfo = await roles.findById(userInfo.data.roleId)
            if (roleInfo.isSuccess) {
                if (roleInfo.data.roleName == "user")
                    await next()
                else {
                    return res.json(responseStatus.BAD_REQUEST)
                }
            } 
            else
                return res.json(responseStatus.BAD_REQUEST)
        } else
            return res.json(responseStatus.BAD_REQUEST)
    },

    checkPermission: async (req, res, next) => {
        console.log("313333333333")
        let userToken = req.headers.token
        let userInfo = await users.findUserByToken(userToken)
        if (userInfo.isSuccess) {
            const roleInfo = await roles.findById(userInfo.data.roleId)
            if (roleInfo.isSuccess) {
                const listPermission = await permissionResource.findByIds(roleInfo.data.permissionResources)
                if (listPermission.data.some(e => e.code == req.url.replace("/","")))
                   await next()
                   else{
                    return res.json(responseStatus.BAD_REQUEST)
                   }
            }else{
                return res.json(responseStatus.BAD_REQUEST)
            }
        } else {
            return res.json(responseStatus.BAD_REQUEST)
        }
    }
}