const agencies = require("../../../db/mongoDB/agencies")

const updateAgency = async(body) =>{
    const resultUpdate = await agencies.updateOne(body._id , body)
    return resultUpdate
}
module.exports = updateAgency;