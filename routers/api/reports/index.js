
const router = require('express').Router();
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const reportBill = require('./reportBill')
// API CREATE BANK 

router.post(routeLink.API_REPORT_BILL, JwtAuthentication.userTokenAuthentication(), Authorization.checkAdmin, async (req,res) => {
    const schema = Joi.object({
        fromDate : Joi.date().allow(null),
        toDate : Joi.date().allow(null),
        userCreated : Joi.objectId().allow(null),
        posId : Joi.objectId().allow(null),
        bankId : Joi.objectId().allow(null),
        cardTypeId : Joi.objectId().allow(null),
        toDate : Joi.date().allow(null),
        status : Joi.number().min(1).max(3).allow(null),
        errorFlag :Joi.number().min(0).max(1).allow(null),
        page : Joi.number().min(0).required(),
    });
    try {
        const value = await schema.validateAsync({
            userCreated : req.body.userCreated,
            posId : req.body.posId,
            bankId : req.body.bankId,
            cardTypeId : req.body.cardTypeId,
            fromDate : req.body.fromDate,
            toDate : req.body.toDate,
             status: req.body.status, 
             errorFlag: req.body.errorFlag, 
             page: req.body.page 
            });
        let list = await reportBill(req.body)
        return res.json(list)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

module.exports = router;