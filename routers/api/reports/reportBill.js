const bills = require("../../../db/mongoDB/bills");
const posmachine  = require("../../../db/mongoDB/posmachine");
const users = require("../../../db/mongoDB/users");
const roles = require("../../../db/mongoDB/roles");

const objectID = require('mongodb').ObjectID;
const responseStatus = require("../../../constants/responseStatus");
const banks = require("../../../db/mongoDB/banks");
const card_types = require("../../../db/mongoDB/card_types");
const reportBill = async (body) =>{
    const billList = await bills.findReport(body)
    let totalMoneyTransaction = 0;
    let totalMoneyBackAccount = 0;
    let totalProfitMoney = 0;
    if(billList.isSuccess)
    {
       
        for(let bill of billList.data)
        {
            totalMoneyTransaction = totalMoneyTransaction + Number(bill.moneybackCutomer)
            totalMoneyBackAccount = totalMoneyBackAccount + Number(bill.moneybackAccount)
            totalProfitMoney = totalProfitMoney + Number(bill.profitMoney)
            bill.createdAt = objectID(bill._id).getTimestamp()
            if(bill.posId)
             {
                 let posInfo = await posmachine.findOne(bill.posId)
                 if(posInfo.isSuccess)
                 {
                    bill.posInfo = posInfo.data
                 }
             }
             if(bill.bankId)
             {
                let bankInfo = await banks.findOne(bill.bankId)
                if(bankInfo.isSuccess)
                {
                   bill.bankInfo = bankInfo.data
                }
             }
             if(bill.cardtypeId)
             {
                let cardInfo = await card_types.findOne(bill.cardtypeId)
                if(cardInfo.isSuccess)
                {
                   bill.cardInfo = cardInfo.data
                }
             }
             if(bill.userId)
             {
                let userInfo = await users.findById(bill.userId,{username:1,fullname:1})
                if(userInfo.isSuccess)
                {
                   bill.userInfo = userInfo.data
                }
             }
        }
    }
   let retrunData = {
     totalMoneyTransaction,
     totalMoneyBackAccount,
     totalProfitMoney,
     billList
   }
    return retrunData
}


module.exports = reportBill;