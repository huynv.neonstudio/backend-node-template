const checkout_dates = require("../../../db/mongoDB/checkout_dates")

const createCheckoutDate = async(body) =>{
    const result= await checkout_dates.insertOne( body)
    return result
}
module.exports = createCheckoutDate;