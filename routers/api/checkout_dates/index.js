
const router = require('express').Router();
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const createCheckoutDate = require('./createCheckoutDate')
const listCheckoutDate = require('./listCheckoutDate')
const updateCheckoutDate = require('./updateCheckoutDate')
// API CREATE BANK 

router.post(routeLink.API_API_CHECKOUT_DATE_CREATE, JwtAuthentication.userTokenAuthentication(), async (req,res) => {
    const schema = Joi.object({
        //  imagePos : Joi.string().required(),
         userId : Joi.objectId().required(),
         description : Joi.string().required(),
         imageAccountant : Joi.array().items({
            imageUrl : Joi.string().required(),
        }).required(),
    });
    try {
        const value = await schema.validateAsync(
            { 
                // imagePos :  req.body.imagePos,
                imageAccountant : req.body.imageAccountant,
                userId : req.body.userId,
                description : req.body.description,
            }
            );
        let checkoutdate = await createCheckoutDate(req.body)
        return res.json(checkoutdate)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_API_CHECKOUT_DATE_LIST, JwtAuthentication.userTokenAuthentication(), async (req,res) => {
    const schema = Joi.object({
        page : Joi.number().min(0).required(),
        fromDate : Joi.date().allow(null),
        toDate : Joi.date().allow(null),
        userId : Joi.objectId(),
    });
    try {
        const value = await schema.validateAsync({
            page : req.body.page,
            fromDate : req.body.fromDate,
            toDate : req.body.toDate,
            userId : req.body.userId,
        });
        let checkoutDate = await listCheckoutDate(req.body)
        return res.json(checkoutDate)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_AGENCY_UPDATE,  async (req,res) => {
    const schema = Joi.object({
        _id:Joi.objectId().required(),
        agencyName : Joi.string().min(4),
        phone : Joi.string().min(8).max(14),
        deleted : Joi.boolean(),

    });
    try {
        const value = await schema.validateAsync(
            { _id: req.body._id,
                agencyName :  req.body.agencyName,
                phone : req.body.phone,
                deleted : req.body.deleted
             }
            );
        let bank = await updateCheckoutDate(req.body)
        return res.json(bank)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

module.exports = router;