const objectID = require('mongodb').ObjectID
const checkout_dates = require("../../../db/mongoDB/checkout_dates");
const { responseStatus } = require('../../../constants')
const listAgency = async (body) =>{
    const list = await checkout_dates.find(body)
    const totalRecord = await checkout_dates.count(body)
    if(list.isSuccess)
    {
        for(let check of list.data)
        {
            check.createdAt =  objectID(check._id).getTimestamp()
        }
        let retrunData = {
            totalRecord : totalRecord.data,
            listCheckout: list.data
           
        }
         return Object.assign({},responseStatus.SUCCESS,{data : retrunData}) 
    }

   
    return list
}
module.exports = listAgency;