
const router = require('express').Router();
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const createPosMachine = require('./createPosMachine')
const listPosMachine = require('./listPosMachine')
const updatePosMachine = require('./updatePosMachine')
const listPosByUser = require('./listPosByUser')
// API CREATE BANK 

router.post(routeLink.API_POS_MACHINE_CREATE, async (req,res) => {
    const schema = Joi.object({
        machineName : Joi.string().min(4).required(),
        bankId : Joi.objectId().required(),
        agencyId :  Joi.objectId().required(),
        moneyLimit :  Joi.number().min(1000).required()
      
    });
    try {
        const value = await schema.validateAsync(
            { 
                machineName :  req.body.machineName,
                bankId : req.body.bankId,
                agencyId : req.body.agencyId,
                moneyLimit : req.body.moneyLimit,
            }
            );
        let pos = await createPosMachine(req.body)
        return res.json(pos)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.get(routeLink.API_POS_MACHINE_LIST, async (req,res) => {
    const schema = Joi.object();
    try {
        const value = await schema.validateAsync();
        let banks = await listPosMachine(req.body)
        return res.json(banks)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

router.get(routeLink.API_POS_LIST_BY_USER, JwtAuthentication.userTokenAuthentication(), async (req,res) => {
    const schema = Joi.object();
    try {
        const value = await schema.validateAsync();
        let poss = await listPosByUser(req.body)
        return res.json(poss)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_POS_MACHINE_UPDATE,  async (req,res) => {
    const schema = Joi.object({
        _id:Joi.objectId().required(),
        machineName : Joi.string().min(4),
        bankId : Joi.objectId(),
        agencyId :  Joi.objectId(),
        moneyLimit :  Joi.number().min(1000),
        deleted : Joi.boolean()
         
    });
    try {
        const value = await schema.validateAsync(
            { _id: req.body._id,
                machineName :  req.body.machineName,
                bankId : req.body.bankId,
                agencyId : req.body.agencyId,
                moneyLimit : req.body.moneyLimit,
                deleted :req.body.deleted

             }
            );
        let bank = await updatePosMachine(req.body)
        return res.json(bank)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

module.exports = router;