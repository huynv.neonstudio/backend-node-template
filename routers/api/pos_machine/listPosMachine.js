const agencies = require("../../../db/mongoDB/agencies");
const posmachine = require("../../../db/mongoDB/posmachine");
const banks = require("../../../db/mongoDB/banks");

const listCardType = async () =>{
    const list = await posmachine.findAll()
    if(list.isSuccess)
    {
        for await(let  card of list.data)
        {
            let bankInfo = await banks.findOne(card.bankId)
            if(bankInfo.isSuccess)
            {
                card.bankInfo = bankInfo.data
            }
            let agencyInfo = await agencies.findOne(card.agencyId)
            if(agencyInfo.isSuccess)
            {
                card.agencyInfo = agencyInfo.data
            }
        }
    }
     
    return list
}
module.exports = listCardType;