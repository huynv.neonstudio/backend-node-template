const agencies = require("../../../db/mongoDB/agencies");
const posmachine = require("../../../db/mongoDB/posmachine");
const banks = require("../../../db/mongoDB/banks");
const users = require("../../../db/mongoDB/users");
const responseStatus = require("../../../constants/responseStatus");

const listPosByUser = async (body) => {
    if (body.userId) {
        let userInfo = await users.findById(body.userId)
        if (userInfo.isSuccess) {
            let posList = []
            for await (let pos of userInfo.data.pos_machines) {
                let posInfo = await posmachine.findOne(pos.posId)
                {
                    if (posInfo.isSuccess) {
                        posList.push(posInfo.data)
                    }
                }
            }
            return Object.assign({},responseStatus.SUCCESS,{data : posList}) 
        }
    } else {
        return responseStatus.NOT_FOUND
    }

}
module.exports = listPosByUser;