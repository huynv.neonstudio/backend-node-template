const posmachine = require("../../../db/mongoDB/posmachine")

const updateCardType = async(body) =>{
    const resultUpdate = await posmachine.updateOne(body._id , body)
    return resultUpdate
}
module.exports = updateCardType;