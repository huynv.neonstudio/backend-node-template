const products = require("../../../db/mongoDB/bills")

const editProduct = async(body) =>{
    let dataInsert =
    {
        errorFlag : body.errorFlag,
    } 
    const resultUpdate = await products.updateOne(body._id , dataInsert)
    return resultUpdate
}
module.exports = editProduct;