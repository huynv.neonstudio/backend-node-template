const responseStatus = require("../../../constants/responseStatus");
const bills = require("../../../db/mongoDB/bills");
const bill_update_logs = require("../../../db/mongoDB/bill_update_logs");
const card_types = require("../../../db/mongoDB/card_types")
const trading_time_frames = require("../../../db/mongoDB/trading_time_frames")
Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
  }
const updateBill = async(body) =>{
    if(body.moneyTransaction)
    {
    let cardInfo = await card_types.findOne(body.cardtypeId)
    if(cardInfo.isSuccess)
    {
        body.moneybackCutomer = Number(body.moneyTransaction) * (100 -Number(cardInfo.data.customerFee))
        body.moneybackAccount = Number(body.moneyTransaction) * (100 -Number(cardInfo.data.accountFee))
        body.profitMoney = body.moneybackAccount - body.moneybackCutomer
    }else {
        return  responseStatus.INTERNAL_SERVER_ERROR
    }
}
if(body.bankId)
{
    let timeFrameInfo =  await trading_time_frames.findByBankId(body.bankId)
    let timeSelect = null;
    if(timeFrameInfo.isSuccess)
    {
       
        for(let timeFrame of timeFrameInfo.data)
        {
            if(timeFrame.startTime < body.timeTransaction && timeFrame.endTime > body.timeTransaction)
            {
                timeSelect = timeFrame
            }
        }
    }
    if(timeSelect)
        body.timeMoneyReturnAccount = new Date(body.dateTransaction + " "+timeSelect.endTime).addHours(timeSelect.durationTime)
    else{
        return Object.assign({},responseStatus.NOT_FOUND,{message:"No time frame found"})
    }
}
    if(body.userId)
    {
        delete body.userId
    }
   
    if(body.dateTransaction)
    body.dateTransaction = new Date(body.dateTransaction).toISOString()
    bill_update_logs.insertOne(body)
    const resultUpdate = await bills.updateOne(body._id , body)
    return resultUpdate
}
module.exports = updateBill;