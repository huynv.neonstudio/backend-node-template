const responseStatus = require("../../../constants/responseStatus")
const bills = require("../../../db/mongoDB/bills")
const card_agency_banks = require("../../../db/mongoDB/card_agency_banks")
const card_types = require("../../../db/mongoDB/card_types")
const trading_time_frames = require("../../../db/mongoDB/trading_time_frames")
Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
  }
const createBills = async(body) =>{
    let cardInfo = await card_agency_banks.findByQuery({
       cardId : body.cardtypeId,
       agencyId :  body.agencyId,
       bankId :  body.bankId,
    })
    if(cardInfo.isSuccess)
    {
        body.moneybackCutomer = Number(body.moneyTransaction) * (100 -Number(cardInfo.data.customerFee))/100
        body.moneybackAccount = Number(body.moneyTransaction) * (100 -Number(cardInfo.data.accountFee))/100
        body.profitMoney = body.moneybackAccount - body.moneybackCutomer
    }else {
        return  Object.assign({},responseStatus.NOT_FOUND,{message:"bank agency card didnt mapping"})
    }
    let timeFrameInfo =  await trading_time_frames.findByBankId(body.bankId)
    let timeSelect = null;
    if(timeFrameInfo.isSuccess)
    {
       
        for(let timeFrame of timeFrameInfo.data)
        {
            if(timeFrame.startTime < body.timeTransaction && timeFrame.endTime > body.timeTransaction)
            {
                timeSelect = timeFrame
            }
        }
    }
    if(timeSelect)
        body.timeMoneyReturnAccount = new Date(body.dateTransaction + " "+timeSelect.endTime).addHours(timeSelect.durationTime)
    else{
        return Object.assign({},responseStatus.NOT_FOUND,{message:"No time frame found"})
    }
    body.errorFlag = 0;
    body.dateTransaction = new Date(body.dateTransaction).toISOString()
    body.status = 1; // chờ duyệt 
    const result= await bills.insertOne( body)
    return result
}
module.exports = createBills;