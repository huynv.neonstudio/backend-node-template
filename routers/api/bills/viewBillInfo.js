const banks = require("../../../db/mongoDB/banks")
const bills = require("../../../db/mongoDB/bills")
const card_types = require("../../../db/mongoDB/card_types")
const posmachine = require("../../../db/mongoDB/posmachine")
const users = require("../../../db/mongoDB/users")
const agencies = require("../../../db/mongoDB/agencies")

const objectID = require('mongodb').ObjectID
const viewBillInfo = async(body) =>{
    const billInfo = await bills.findOne(body._id)
     if(billInfo.isSuccess)
     {
        billInfo.data.createdAt = objectID(billInfo.data._id).getTimestamp()
         let bankInfo = await banks.findOne(billInfo.data.bankId)
         if(bankInfo.isSuccess) billInfo.data.bankInfo = bankInfo.data

         let posInfo = await posmachine.findOne(billInfo.data.posId)
         if(posInfo.isSuccess) billInfo.data.posInfo = posInfo.data

         let cardInfo = await card_types.findOne(billInfo.data.cardtypeId)
         if(cardInfo.isSuccess) billInfo.data.cardInfo = cardInfo.data

         let userInfo = await users.findById(billInfo.data.userId)
         if(userInfo.isSuccess) 
         { 
             let userData = userInfo.data
             delete userData.password
             delete userData.token
             delete userData.refreshToken
             billInfo.data.userInfo = userData
         }
         let agencyInfo = await agencies.findOne(billInfo.data.agencyId)
         if(agencyInfo.isSuccess) billInfo.data.agencyInfo = agencyInfo.data

     }
    return billInfo
}
module.exports = viewBillInfo;