
const router = require('express').Router();
const Joi = require('@hapi/joi').extend(require('@joi/date'));
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const accountantConfirm = require('./accountantConfirm');
const makeErorrFlag = require('./makeErorrFlag');
const createBills = require('./createBill');
const listBill = require('./listBill');
const viewBillInfo = require('./viewBillInfo');
const updateBill = require('./updateBill');

const objectID = require('mongodb').ObjectID
    
router.post(routeLink.API_BILL_LIST, JwtAuthentication.userTokenAuthentication(), async (req, res) => {
    const schema = Joi.object({
        fromDate : Joi.date().allow(null),
        toDate : Joi.date().allow(null),
        status : Joi.number().min(1).max(3).allow(null),
        errorFlag :Joi.number().min(0).max(1).allow(null),
        page : Joi.number().min(0).required(),
    });
    try {
        const value = await schema.validateAsync({
            fromDate : req.body.fromDate,
            toDate : req.body.toDate,
             status: req.body.status, 
             errorFlag: req.body.errorFlag, 
             page: req.body.page 
            });
        let list = await listBill(req.body)
        return res.json(list)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }

})

router.post(routeLink.API_BILL_CREATE, JwtAuthentication.userTokenAuthentication(), async (req,res) => {
    const schema = Joi.object({
        dateTransaction : Joi.date().required(),
        timeTransaction : Joi.date().format('HH:mm:ss').raw(),
        bankId :  Joi.objectId().required(),
        posId :  Joi.objectId().required(),
        agencyId : Joi.objectId().required(),
        cardtypeId :  Joi.objectId().required(),
        userId:Joi.objectId().required(),
        moneyTransaction : Joi.number(),
        noteTeller : Joi.string().allow(""),
        intermediaryAccount : Joi.string().required(),
        imageTeller : Joi.array().items({
            imageUrl : Joi.string().required(),
        }).required(),
    });
    try {
        const value = await schema.validateAsync(
            { 
                dateTransaction : req.body.dateTransaction,
                timeTransaction : req.body.timeTransaction,
                bankId : req.body.bankId,
                posId : req.body.posId,
                agencyId : req.body.agencyId,
                intermediaryAccount : req.body.intermediaryAccount,
                cardtypeId : req.body.cardtypeId,
                noteTeller : req.body.noteTeller,
                imageTeller : req.body.imageTeller,
                moneyTransaction : req.body.moneyTransaction,
                userId : req.body.userId,
             }
            );
        let bill = await createBills(req.body)
        return res.json(bill)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

router.post(routeLink.API_BILL_ACCOUNT_CONFIRM, async (req,res) => {
    const schema = Joi.object({
        _id : Joi.objectId().required(),
        status : Joi.number().min(1).max(3).required(),
        noteAccountant : Joi.string().allow(""),
        imageAccountant : Joi.array().items({
            imageUrl : Joi.string().required(),
        }).required(),
    });
    try {
        const value = await schema.validateAsync(
            { 
                _id : req.body._id,
                status : req.body.status,
                noteAccountant : req.body.noteAccountant,
                imageAccountant : req.body.imageAccountant,
             }
            );
        let bill = await accountantConfirm(req.body)
        return res.json(bill)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

router.post(routeLink.API_BILL_UPDATE, JwtAuthentication.userTokenAuthentication(),  Authorization.checkAdmin, async (req,res) => {
    const schema = Joi.object({
        _id : Joi.objectId().required(),
        dateTransaction : Joi.date(),
        timeTransaction : Joi.date().format('HH:mm:ss').raw(),
        bankId :  Joi.objectId(),
        posId :  Joi.objectId(),
        agencyId : Joi.objectId(),
        intermediaryAccount : Joi.string(),
        cardtypeId :  Joi.objectId(),
        // userId:Joi.objectId().required(),
        moneyTransaction : Joi.number(),
        noteTeller : Joi.string(),
        imageTeller : Joi.array().items({
            imageUrl : Joi.string().required(),
        }),
        status : Joi.number().min(1).max(3),
        noteAccountant : Joi.string(),
        imageAccountant : Joi.array().items({
            imageUrl : Joi.string().required(),
        }),
    }).and("cardtypeId","moneyTransaction");
    try {
        const value = await schema.validateAsync(
            { 
                _id : req.body._id,
                dateTransaction : req.body.dateTransaction,
                timeTransaction : req.body.timeTransaction,
                bankId :  req.body.bankId,
                posId :  req.body.posId,
                agencyId : req.body.agencyId,
                cardtypeId :  req.body.cardtypeId,
                intermediaryAccount : req.body.intermediaryAccount,
                // userId:req.body.userId,
                moneyTransaction : req.body.moneyTransaction,
                noteTeller : req.body.noteTeller,
                imageTeller : req.body.imageTeller,
                status : req.body.status,
                noteAccountant : req.body.noteAccountant,
                imageAccountant : req.body.imageAccountant,
             }
            );
        let bill = await updateBill(req.body)
        return res.json(bill)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

router.post(routeLink.API_BILL_MAKE_ERROR_FLAG, async (req,res) => {
    const schema = Joi.object({
        _id : Joi.objectId().required(),
        errorFlag : Joi.number().min(0).max(1).required(),
    });
    try {
        const value = await schema.validateAsync(
            { 
                _id : req.body._id,
                errorFlag : req.body.errorFlag,
             }
            );
        let bill = await makeErorrFlag(req.body)
        return res.json(bill)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

router.post(routeLink.API_VIEW_BILL_INFO, async (req,res) => {
    const schema = Joi.object({
        _id: Joi.objectId().required(),
    });
    try {
        const value = await schema.validateAsync({ _id: req.body._id });
        let product = await viewBillInfo(req.body)
        return res.json(product)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_PRODUCT_EDIT, Authorization.checkAdmin, async (req,res) => {
    const schema = Joi.object({
        _id: Joi.string().min(10).required(),
        name : Joi.string().min(4).required(),
        descriptions : Joi.string().min(4).required(),
        image : Joi.string().min(4).required(),
    });
    try {
        const value = await schema.validateAsync(
            { _id: req.body._id,
                name :  req.body.name,
                descriptions : req.body.descriptions,
                image : req.body.image
             }
            );
        let product = await editProduct(req.body)
        return res.json(product)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
// router.post(routeLink.API_PRODUCT_EDIT,
//     JwtAuthentication.userTokenAuthentication() , 
//     async (req, res) => {
//     let resultLogin = await getUserInfo(req)
//     return res.json(resultLogin)
// })

module.exports = router;