const bills = require("../../../db/mongoDB/bills");
const posmachine  = require("../../../db/mongoDB/posmachine");
const users = require("../../../db/mongoDB/users");
const roles = require("../../../db/mongoDB/roles");

const objectID = require('mongodb').ObjectID;
const responseStatus = require("../../../constants/responseStatus");
const banks = require("../../../db/mongoDB/banks");
const card_types = require("../../../db/mongoDB/card_types");
const list = async (body) =>{
    if(body.userId)
        { 
            let userInfo = await users.findById(body.userId)
            if(userInfo.isSuccess)
            {
                let roleInfo = await roles.findById(userInfo.data.roleId)
                if(roleInfo.isSuccess)
                {
                    if(roleInfo.data.roleCode == "accountant"  && userInfo.data.pos_machines && userInfo.data.pos_machines.length>0)
                    {
                        body.pos_machineIds =  userInfo.data.pos_machines.map(pos=>pos.posId)
                    }else if(roleInfo.data.roleCode == "tellers")
                    {
                        body.userCreated = body.userId
                    }
                }else{
                    return responseStatus.NOT_FOUND
                }
            }else{
                return responseStatus.NOT_FOUND
            }
        }

    const billList = await bills.find(body)
    const totalRecord = await bills.count(body)
    if(billList.isSuccess)
    {
        for(let bill of billList.data)
        {
            bill.createdAt = objectID(bill._id).getTimestamp()
            if(bill.posId)
             {
                 let posInfo = await posmachine.findOne(bill.posId)
                 if(posInfo.isSuccess)
                 {
                    bill.posInfo = posInfo.data
                 }
             }
             if(bill.bankId)
             {
                let bankInfo = await banks.findOne(bill.bankId)
                if(bankInfo.isSuccess)
                {
                   bill.bankInfo = bankInfo.data
                }
             }
             if(bill.cardtypeId)
             {
                let cardInfo = await card_types.findOne(bill.cardtypeId)
                if(cardInfo.isSuccess)
                {
                   bill.cardInfo = cardInfo.data
                }
             }
        }
        let retrunData = {
            totalRecord : totalRecord.data,
            listBill: billList.data
           
        }
         return Object.assign({},responseStatus.SUCCESS,{data : retrunData}) 
         
    }else{
        return responseStatus.NOT_FOUND
    }
    

}


module.exports = list;