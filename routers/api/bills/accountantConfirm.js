const products = require("../../../db/mongoDB/bills")

const editProduct = async(body) =>{
    let dataInsert =
    {
        imageAccountant : body.imageAccountant,
        noteAccountant : body.noteAccountant,
        status : body.status,
    } 
    const resultUpdate = await products.updateOne(body._id , dataInsert)
    return resultUpdate
}
module.exports = editProduct;