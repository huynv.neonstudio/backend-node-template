const card_types = require("../../../db/mongoDB/card_types")

const createCardType = async(body) =>{
    const result= await card_types.insertOne( body)
    return result
}
module.exports = createCardType;