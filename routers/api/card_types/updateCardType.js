const card_types = require("../../../db/mongoDB/card_types")

const updateCardType = async(body) =>{
    const resultUpdate = await card_types.updateOne(body._id , body)
    return resultUpdate
}
module.exports = updateCardType;