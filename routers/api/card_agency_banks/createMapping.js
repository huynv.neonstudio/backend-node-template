const card_agency_banks = require("../../../db/mongoDB/card_agency_banks")

const CreateMapping = async(body) =>{
    const result= await card_agency_banks.insertOne(body)
    return result
}
module.exports = CreateMapping;