
const router = require('express').Router();
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const createMapping = require('./createMapping')
const listMapping = require('./listMapping')
const updateMapping = require('./updateMapping')
// API CREATE BANK 

router.post(routeLink.API_CARD_AGENCY_BANK_MAPPING_CREATE, async (req,res) => {
    const schema = Joi.object({
        // cardTypeName : Joi.string().min(4).required(),
        bankId : Joi.objectId().required(),
        agencyId :  Joi.objectId().required(),
        cardId :  Joi.objectId().required(),
        customerFee : Joi.number().min(0).required(),
        accountFee : Joi.number().min(0).required(),
        
    });
    try {
        const value = await schema.validateAsync(
            { 
                // cardTypeName :  req.body.cardTypeName,
                bankId : req.body.bankId,
                cardId : req.body.cardId,
                agencyId : req.body.agencyId,
                customerFee : req.body.customerFee,
                accountFee : req.body.accountFee,
            }
            );
        let card = await createMapping(req.body)
        return res.json(card)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_CARD_AGENCY_BANK_MAPPING_LIST, async (req,res) => {
    const schema = Joi.object({
         bankId : Joi.objectId(),  
         cardId : Joi.objectId(),  
         agencyId : Joi.objectId(),
        //  page :   Joi.number().min(1).required(),
    });
    try {
        const value = await schema.validateAsync({
             bankId : req.body.bankId,
             agencyId : req.body.agencyId,
             cardId : req.body.cardId,
            //  page : req.body.page,
        });
        let banks = await listMapping(req.body)
        return res.json(banks)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_CARD_AGENCY_BANK_MAPPING_UPDATE,  async (req,res) => {
    const schema = Joi.object({
        _id:Joi.objectId().required(),
        // cardTypeName : Joi.string().min(4),
        bankId : Joi.objectId(),
        agencyId :  Joi.objectId(),
        cardId :  Joi.objectId(),
        customerFee : Joi.number().min(0),
        accountFee : Joi.number().min(0),
        deleted : Joi.boolean()
    });
    try {
        const value = await schema.validateAsync(
            { _id: req.body._id,
                // cardTypeName :  req.body.cardTypeName,
                bankId : req.body.bankId,
                agencyId : req.body.agencyId,
                cardId : req.body.cardId,
                customerFee : req.body.customerFee,
                accountFee : req.body.accountFee,
                deleted : req.body.deleted
             }
            );
        let bank = await updateMapping(req.body)
        return res.json(bank)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

module.exports = router;