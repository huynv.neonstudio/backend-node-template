const agencies = require("../../../db/mongoDB/agencies");
const banks = require("../../../db/mongoDB/banks");
const card_agency_banks = require("../../../db/mongoDB/card_agency_banks");
const card_types = require("../../../db/mongoDB/card_types");
const listMapping = async (body) =>{
    const list = await card_agency_banks.find(body)
    if(list.isSuccess)
    {
        for await(let  cardMap of list.data)
        {
            let bankInfo = await banks.findOne(cardMap.bankId)
            if(bankInfo.isSuccess)
            {
                cardMap.bankInfo = bankInfo.data
            }
            let agencyInfo = await agencies.findOne(cardMap.agencyId)
            if(agencyInfo.isSuccess)
            {
                cardMap.agencyInfo = agencyInfo.data
            }
            let cardInfo = await card_types.findOne(cardMap.cardId)
            if(cardInfo.isSuccess)
            {
                cardMap.cardInfo = cardInfo.data
            }
        }
    }
     
    return list
}
module.exports = listMapping;