const card_agency_banks = require("../../../db/mongoDB/card_agency_banks")

const updateMapping = async(body) =>{
    const resultUpdate = await card_agency_banks.updateOne(body._id , body)
    return resultUpdate
}
module.exports = updateMapping;