const router = require('express').Router();
const routeLink = require('../../constants/routeLink');
const users = require('./user')
const bills = require('./bills');
const utils = require('./utils')
const banks = require('./banks')
const card_types = require('./card_types')
const agencies = require('./agencies')
const pos_machine = require('./pos_machine')
const roles = require('./roles')
const reports = require('./reports')
const trading_time_frames = require('./trading_time_frames')
const checkout_dates = require('./checkout_dates')
const card_agency_banks = require('./card_agency_banks')
const Authorization = require('../../utils/Authorization');
const { checkAdmin } = require('../../utils/Authorization');

router.use(routeLink.API_USER,users)
router.use(routeLink.API_ROLE,roles)
router.use(routeLink.API_BILL, bills)
router.use(routeLink.API_UTILS, utils)
router.use(routeLink.API_BANK, banks)
router.use(routeLink.API_CARD_TYPE, card_types)
router.use(routeLink.API_AGENCY, agencies)
router.use(routeLink.API_TRADING_TIME_FRAME, trading_time_frames)
router.use(routeLink.API_CHECKOUT_DATE, checkout_dates)

router.use(routeLink.API_POS_MACHINE, pos_machine)

router.use(routeLink.API_REPORT, reports)
router.use(routeLink.API_CARD_AGENCY_BANK, card_agency_banks)


module.exports = router;