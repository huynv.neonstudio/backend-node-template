const agencies = require("../../../db/mongoDB/agencies")

const createAgency = async(body) =>{
    const result= await agencies.insertOne( body)
    return result
}
module.exports = createAgency;