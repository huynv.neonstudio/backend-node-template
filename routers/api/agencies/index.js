
const router = require('express').Router();
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const createAgency = require('./createAgency')
const listAgency = require('./listAgency')
const updateAgency = require('./updateAgency')
// API CREATE BANK 

router.post(routeLink.API_AGENCY_CREATE, async (req,res) => {
    const schema = Joi.object({
        agencyName : Joi.string().min(4).required(),
        phone : Joi.string().min(8).max(14).required(),
    });
    try {
        const value = await schema.validateAsync(
            { 
                agencyName :  req.body.agencyName,
                phone : req.body.phone,
            }
            );
        let agency = await createAgency(req.body)
        return res.json(agency)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.get(routeLink.API_AGENCY_LIST, async (req,res) => {
    const schema = Joi.object();
    try {
        const value = await schema.validateAsync();
        let banks = await listAgency(req.body)
        return res.json(banks)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_AGENCY_UPDATE,  async (req,res) => {
    const schema = Joi.object({
        _id:Joi.objectId().required(),
        agencyName : Joi.string().min(4),
        phone : Joi.string().min(8).max(14),
        deleted : Joi.boolean(),

    });
    try {
        const value = await schema.validateAsync(
            { _id: req.body._id,
                agencyName :  req.body.agencyName,
                phone : req.body.phone,
                deleted : req.body.deleted
             }
            );
        let bank = await updateAgency(req.body)
        return res.json(bank)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

module.exports = router;