const { responseStatus }= require('../../../constants')
const mongoDB = require('../../../db/mongoDB')

getUserInfo = async (userData) => {
    let userInfo = await mongoDB.users.findById(userData.body.userId)
    if (userInfo.isSuccess) {
        delete userInfo.data.token
        delete userInfo.data.refreshToken
        delete userInfo.data.password
        return userInfo
    }
    else {
        return responseStatus.NOT_FOUND
    }
}
module.exports = getUserInfo;