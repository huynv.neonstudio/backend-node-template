
const fs = require('fs')
const JwtAuthentication = require('../../../utils/JwtAuthentication')
const path = require('path')
const jwt = require('jsonwebtoken')
const mongoDB = require('../../../db/mongoDB')
const responseStatus = require('../../../constants/responseStatus')
const consts = require('../../../constants/consts')
refreshToken = async (data)=>{
    if(!data.refreshToken)
    {
        return responseStatus.BAD_REQUEST
    }
    else { 
        let userInfo = await mongoDB.users.findUserByRefreshToken(data.refreshToken)
        if(userInfo.isSuccess)
        {
            if(userInfo.data.refreshToken == data.refreshToken)
            {
                let resultVerifyToken = await JwtAuthentication.verifyToken(data.refreshToken)
                if(resultVerifyToken){
                    jwtPayload = {
                        username : userInfo.data.username
                    }
                    jwtPayloadRefresh = {
                        _id : userInfo.data._id
                    }
                    const absolutePath = path.resolve(__dirname + "./../../../jwt_key/jwtRS256.key")
                    const cert = fs.readFileSync(absolutePath)
                    const token = jwt.sign(jwtPayload, cert, { algorithm: 'RS256', expiresIn: consts.TIME_JWT_TOKEN_EXPIRE });
                    // const refreshToken = jwt.sign(jwtPayloadRefresh, cert, { algorithm: 'RS256', expiresIn: constants.TIME_JWT_REFRESH_TOKEN_EXPIRE });
                    let updateToken = await mongoDB.users.updateUser(userInfo.data._id,{token : token})
                    return Object.assign({},responseStatus.SUCCESS,{token : token})
                }else{
                    return responseStatus.BAD_REQUEST
                }
            }else{
                return responseStatus.BAD_REQUEST
            }
        }else{
            return responseStatus.BAD_REQUEST
        }
    }
}
module.exports = refreshToken;