const md5 = require('md5')
const { responseStatus } = require('../../../constants')
const mongoDB = require('../../../db/mongoDB')

updateUser = async (userData) => {
    let userExisted = await mongoDB.users.findById(userData._id)
    if (userExisted.isSuccess) {
        if(userData.password)
        userData.password = md5(userData.password)
        let userInfo = await mongoDB.users.updateUser(userData._id,userData)
        return userInfo
    } else {
        return responseStatus.NOT_FOUND
    }

}
module.exports = updateUser;