const users = require("../../../db/mongoDB/users");
const responseStatus = require("../../../constants/responseStatus");
const roles = require("../../../db/mongoDB/roles");
const posmachine = require("../../../db/mongoDB/posmachine");
const moment = require("moment");

const listUsers = async (body) =>{
   let list = await users.find(body)
   if(list.isSuccess)
   {
       for await(let usr of list.data)
       {
           let roleInfo = await roles.findById(usr.roleId)
           if(roleInfo.isSuccess)
           {
           usr.roleInfo = roleInfo.data
        }
           for await(let pos of usr.pos_machines)
           {
               let posInfo = await posmachine.findOne(pos.posId)
               if(posInfo.isSuccess)
               {
                pos.posInfo = posInfo.data
               }
               
           }
       }
   }
    return list
}
module.exports = listUsers;