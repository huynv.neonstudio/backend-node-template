const {mongoDB} = require('../../../db')
const { consts , responseStatus } = require('../../../constants')
const path = require('path')
const md5 = require('md5')
const fs = require('fs')
const jwt = require('jsonwebtoken')

const roles = require('../../../db/mongoDB/roles')
const login = async (userData) => {
    let userExisted = await mongoDB.users.findByUsername(userData.username)
    if (userExisted.isSuccess && userExisted.data.active) {
         let roleInfo = await roles.findById(userExisted.data.roleId)
         if(roleInfo.isSuccess )
         {
             userExisted.data.roleInfo = roleInfo.data
         }
        if (userExisted.data.password == md5(userData.password)) {
            jwtPayload = {
                username : userData.username
            }
            jwtPayloadRefresh = {
                _id : userExisted.data._id
            }
            console.log(__dirname)
            const absolutePath = path.resolve(__dirname + "./../../../jwt_key/jwtRS256.key")
            const cert = fs.readFileSync(absolutePath)
            const token = jwt.sign(jwtPayload, cert, { algorithm: 'RS256', expiresIn: consts.TIME_JWT_TOKEN_EXPIRE });
            const refreshToken = jwt.sign(jwtPayloadRefresh, cert, { algorithm: 'RS256', expiresIn: consts.TIME_JWT_REFRESH_TOKEN_EXPIRE });
            let updateRefreshToken = await mongoDB.users.updateUser(userExisted.data._id,{refreshToken:refreshToken, token : token})
            console.log(token)
            delete userExisted.data.password
            userExisted.data.token = token
            userExisted.data.refreshToken = refreshToken
            return Object.assign({},responseStatus.SUCCESS,
                {data : userExisted.data}
                )
        }else {
            return responseStatus.NOT_FOUND
        }
    } else {
        return responseStatus.NOT_FOUND
    }
}

module.exports = login;