const md5 = require('md5')
const { responseStatus } = require('../../../constants')
const mongoDB = require('../../../db/mongoDB')

createUser = async (userData) => {
    let userExisted = await mongoDB.users.findByUsername(userData.username)
    if (!userExisted.isSuccess) {
        userData.password = md5(userData.password)
        let userInfo = await mongoDB.users.insertOne(userData)
        return userInfo
    } else {
        return responseStatus.REQUEST_CONFLICT
    }

}
module.exports = createUser;