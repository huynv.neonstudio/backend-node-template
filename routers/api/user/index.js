
const router = require('express').Router();
const Joi = require('@hapi/joi');
const {routeLink} = require('../../../constants');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const login = require('./login')

const getUserInfo = require('./getUserInfo')
const refreshToken  = require('./refreshToken');
const createUser = require('./createUser')
const listUser = require('./listUser')
const updateUser = require('./updateUser')
router.post(routeLink.API_USER_CREATE, async (req, res) => {
    const schema = Joi.object({
        username: Joi.string().min(4).required(),
        password: Joi.string().min(5).required(),
        phone : Joi.string().min(8).max(14).required(),
        CMND : Joi.string().min(6).required(),
        birthday : Joi.date().required(),
        fullname : Joi.string().required(),
        active : Joi.bool().required(),
        roleId : Joi.objectId().required(),
        pos_machines : Joi.array().items({
            posId: Joi.objectId().required(),
        }),
    });
    try {
        const value = await schema.validateAsync(
            { username: req.body.username, password: req.body.password ,
                phone : req.body.phone,
                CMND : req.body.CMND,
                roleId : req.body.roleId,
                birthday : req.body.birthday,
                fullname : req.body.fullname,
                active : req.body.active,
                pos_machines : req.body.pos_machines,
            }
            );
        let result = await createUser(req.body)
        return res.json(result)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }

})


router.post(routeLink.API_USER_UPDATE, async (req, res) => {
    const schema = Joi.object({
        _id : Joi.objectId().required(),
        password: Joi.string().min(5),
        phone : Joi.string().min(8).max(14),
        CMND : Joi.string().min(6),
        birthday : Joi.date(),
        fullname : Joi.string(),
        active : Joi.bool(),
        roleId : Joi.objectId(),
        pos_machines : Joi.array().items({
            posId: Joi.objectId().required(),
        }),
    });
    try {
        const value = await schema.validateAsync(
            { 
                _id : req.body._id,
                password: req.body.password ,
                phone : req.body.phone,
                birthday : req.body.birthday,
        fullname : req.body.fullname,
        active : req.body.active,
                CMND : req.body.CMND,
                roleId : req.body.roleId,
                pos_machines : req.body.pos_machines,
            }
            );
        let result = await updateUser(req.body)
        return res.json(result)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }

})

router.post(routeLink.API_LOGIN, async (req, res) => {
    const schema = Joi.object({
        username: Joi.string().min(4).required(),
        password: Joi.string().min(5).required()
    });
    try {
        const value = await schema.validateAsync({ username: req.body.username, password: req.body.password });
        let resultLogin = await login(req.body)
        return res.json(resultLogin)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }

})

router.post(routeLink.API_USER_LIST, async (req, res) => {
    const schema = Joi.object({
        search : Joi.string().allow(""),
        // page : Joi.number().min(0).required(),
    });
    try {
        const value = await schema.validateAsync({
             search: req.body.search, 
            //  page: req.body.page 
            });
        let resultLogin = await listUser(req.body)
        return res.json(resultLogin)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }

})

router.post(routeLink.API_REFRESH_TOKEN, async (req, res) => {
    let reqData = req.body;
    const schema = Joi.object({
        refreshToken: Joi.string().min(6).required(),
    });
    try {
        const value = await schema.validateAsync({ refreshToken: req.body.refreshToken });
        let resultRegister = await refreshToken(req.body)
        return res.json(resultRegister)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }

})
router.post(routeLink.API_GET_USER_INFO,
    JwtAuthentication.userTokenAuthentication() , 
    async (req, res) => {
    let resultLogin = await getUserInfo(req)
    return res.json(resultLogin)
})

module.exports = router;