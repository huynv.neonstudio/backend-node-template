const banks = require("../../../db/mongoDB/banks");
const listBank = async () =>{
    const list = await banks.findAll()
    return list
}
module.exports = listBank;