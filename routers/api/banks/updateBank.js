const banks = require("../../../db/mongoDB/banks")

const updateBank = async(body) =>{
    const resultUpdate = await banks.updateOne(body._id , body)
    return resultUpdate
}
module.exports = updateBank;