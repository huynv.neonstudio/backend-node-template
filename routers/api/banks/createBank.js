const banks = require("../../../db/mongoDB/banks")

const createBank = async(body) =>{
    const result= await banks.insertOne( body)
    return result
}
module.exports = createBank;