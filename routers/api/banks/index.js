
const router = require('express').Router();
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const createBank = require('./createBank')
const listBank = require('./listBank')
const updateBank = require('./updateBank')
// API CREATE BANK 

router.post(routeLink.API_BANK_CREATE, async (req,res) => {
    const schema = Joi.object({
        bankCode : Joi.string().min(3).required(),
        bankName : Joi.string().min(4).required(),
        logo :  Joi.string().min(4).required(),
    });
    try {
        const value = await schema.validateAsync(
            { 
                bankCode :  req.body.bankCode,
                bankName : req.body.bankName,
                logo : req.body.logo,
            }
            );
        let bill = await createBank(req.body)
        return res.json(bill)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.get(routeLink.API_BANK_LIST, async (req,res) => {
    const schema = Joi.object();
    try {
        const value = await schema.validateAsync();
        let banks = await listBank(req.body)
        return res.json(banks)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_BANK_UPDATE,  async (req,res) => {
    const schema = Joi.object({
        _id:Joi.objectId().required(),
        bankCode : Joi.string().min(3),
        bankName : Joi.string().min(4),
        logo : Joi.string().min(4),
        deleted : Joi.boolean(),
    });
    try {
        const value = await schema.validateAsync(
            { _id: req.body._id,
                bankCode :  req.body.bankCode,
                bankName : req.body.bankName,
                logo : req.body.logo,
                deleted : req.body.deleted
             }
            );
        let bank = await updateBank(req.body)
        return res.json(bank)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

module.exports = router;