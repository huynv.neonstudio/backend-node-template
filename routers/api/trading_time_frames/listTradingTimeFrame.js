const banks = require("../../../db/mongoDB/banks");
const trading_time_frames = require("../../../db/mongoDB/trading_time_frames");

const listTradingTimeFrame = async () =>{
    const list = await trading_time_frames.findAll()
    if(list.isSuccess)
    {
        for await(let  data of list.data)
        {
            let bankInfo = await banks.findOne(data.bankId)
            if(bankInfo.isSuccess)
            {
                data.bankInfo = bankInfo.data
            }
        }
    }
     
    return list
}
module.exports = listTradingTimeFrame;