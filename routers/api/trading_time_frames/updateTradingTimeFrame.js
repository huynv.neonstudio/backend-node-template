const trading_time_frames = require("../../../db/mongoDB/trading_time_frames");

const updateTradingTimeFrame = async(body) =>{
    const resultUpdate = await trading_time_frames.updateOne(body._id , body)
    return resultUpdate
}
module.exports = updateTradingTimeFrame;