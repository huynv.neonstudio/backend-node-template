
const router = require('express').Router();
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi)
const {routeLink} = require('../../../constants');
const Authorization = require('../../../utils/Authorization');
const JwtAuthentication = require('../../../utils/JwtAuthentication');
const createTradingTimeFrame = require('./createTradingTimeFrame')
const listTradingTimeFrame = require('./listTradingTimeFrame')
const updateTradingTimeFrame = require('./updateTradingTimeFrame')
// API CREATE BANK 

router.post(routeLink.API_TRADING_TIME_FRAME_CREATE, async (req,res) => {
    const schema = Joi.object({
        bankId : Joi.objectId().required(),
        startTime :  Joi.string().min(1).required(),
        endTime : Joi.string().min(1).required(),
        durationTime : Joi.number().min(0).required(),
    });
    try {
        const value = await schema.validateAsync(
            {   
                bankId : req.body.bankId,
                startTime : req.body.startTime,
                endTime : req.body.endTime,
                durationTime : req.body.durationTime,
            }
            );
        let card = await createTradingTimeFrame(req.body)
        return res.json(card)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.get(routeLink.API_TRADING_TIME_FRAME_LIST, async (req,res) => {
    const schema = Joi.object();
    try {
        const value = await schema.validateAsync();
        let banks = await listTradingTimeFrame(req.body)
        return res.json(banks)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})
router.post(routeLink.API_TRADING_TIME_FRAME_UPDATE,  async (req,res) => {
    const schema = Joi.object({
        _id:Joi.objectId().required(),
        bankId : Joi.objectId(),
        startTime :  Joi.string().min(1),
        endTime : Joi.string().min(1),
        durationTime : Joi.number().min(0),
        deleted : Joi.boolean()
    });
    try {
        const value = await schema.validateAsync(
            { _id: req.body._id,
                startTime :  req.body.startTime,
                bankId : req.body.bankId,
                endTime : req.body.endTime,
                durationTime : req.body.durationTime,
                deleted : req.body.deleted
             }
            );
        let bank = await updateTradingTimeFrame(req.body)
        return res.json(bank)
    }
    catch (err) {
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
    
})

module.exports = router;