/*
     URL: v1/util/uploadavatar
     METHOD: POST
     TYPE: multipart/form-data 
     USERFIELD: TÙY CHỌN, KHÔNG GIỚI HẠN
     FILE LIMIT: PNG, JPG, JPEG, GIF
*/
const multer = require('multer');
const path = require('path')
const constants = require('../../../constants')
const customStorageEngine = require('../../../utils/FileUploadCustomStorageEngine')
var listUploadFileStatus = []
var storage = customStorageEngine(
    (
    {
    // destination: function (req, file, cb) {
    //     var ext = path.extname(file.originalname)
    //     var newFileName = randomFileID()
    //     var currentFileName = Date.now() + newFileName + ext
    //     cb(null, constants.SERVER_LOCATION_FOR_STORAGE + currentFileName)
    // }
}
)
)
function randomFileID() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 12; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

var imageUpload = multer({
    limits: { fileSize: constants.consts.MAX_SIZE_UPLOAD*1024*1024 },
    storage: storage, 
    fileFilter: function (req, file, callback) {
        // console.log(file)
        var ext = path.extname(file.originalname).toLocaleLowerCase();
        if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.heic') {
            let fileInfo = {
                isSuccess: false,
                fileName: file.originalname,
                filepath : null,
                reason: constants.consts.ERROR_APP_UPLOAD_FILE_NOT_ALLOW
            }
            listUploadFileStatus.push(fileInfo)
            return callback(null, false)
        } else {
            return callback(null, true)
        }
    }
    
}).any()

module.exports = imageUpload