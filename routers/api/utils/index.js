const router = require('express').Router();
const constants = require('../../../constants')
const imageUpload = require('./imageUpload')
const validateToken = require('./validateToken')
const Functions = require('../../../utils/Functions')

const Joi = require('@hapi/joi');
const JwtAuthentication = require('../../../utils/JwtAuthentication')
const multer = require('multer');
// create user data // register 
listUploadFileStatus = []
router.post(constants.routeLink.API_VALIDATE_TOKEN,JwtAuthentication.userTokenAuthentication(), async (req, res) =>{
    const schema = Joi.object({
        token : Joi.string().min(6).required(),
    });
    try {
        const value = await schema.validateAsync({token: req.body.token});
        let resultvalidateToken = await validateToken.validateToken(req.body)
        return res.json(resultvalidateToken)
    }
    catch (err) { 
        console.log(err)
        return res.json({
            isSuccess: false,
            status: 400,
            data: {},
            message: err.message.replace(/[^\w\s]/gi, '')
        })
    }
})
router.post(constants.routeLink.API_IMAGE_UPLOAD
, async (req, res) =>{
    imageUpload(req,res,async function(err){
        console.log(err)
        let listUploadFileStatus = []
        if(err&&err.code=="LIMIT_FILE_SIZE")
            return res.json(constants.responseStatus.BAD_REQUEST)
        for await(let item of req.files)
        {
        var fileI = Functions.getDetailImageFromUrl(item.path)
        // var thumbnailUrl = Functions.getDetailImageFromUrl(item.path, item.thumbnailPath).thumbnailUrl
        // let resultSaveImage = await mongoModel.imageLibs.insert({
        //     fileName: item.originalname,
        //     imagePath: item.path,
        //     // thumbnailPath : item.thumbnailPath
        // })
      
        let fileInfo = {
            isSuccess: true,
            fileName: item.originalname,
            fileURL: fileI.fileURL,
            filePath: fileI.filePath,
            // thumbnailUrl : thumbnailUrl
        }
        // if(resultSaveImage.isSuccess)
        // fileInfo.imageId = resultSaveImage.data._id
         listUploadFileStatus.push(fileInfo)
    };
    body = {...constants.responseStatus.SUCCESS}
    body.data = listUploadFileStatus
    // listUploadFileStatus = []
    return  res.json(body)
})
})
  
module.exports = router;