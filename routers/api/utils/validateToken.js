const JwtAuthentication = require('../../../utils/JwtAuthentication')
const constants = require('../../../constants')
const mongoModel = require('../../../db')

validateToken = async (data)=>{
    if(data.token)
    {
    let userToken = await mongoModel.mongoDB.users.findOne(null,data.userId,null)
    if(userToken.isSuccess)
    {
    let result = await JwtAuthentication.verifyToken(data.token)
    if(result && userToken.data.token == data.token){
       return {
            isSuccess: true,
            status: 200,
            data: {},
            message: constants.consts.TOKEN_VALIDED
        }
    }else {
        return {
            isSuccess: false,
            status: 400,
            data: {},
            message: constants.consts.ERROR_TOKEN_EXPIRED
        }
    }
}else{
    return {
        isSuccess: false,
        status: 400,
        data: {},
        message: constants.consts.USER_DATA_DIDNT_EXISTED
    }
}
}
}
module.exports.validateToken = validateToken