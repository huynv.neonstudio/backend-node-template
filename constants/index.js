const constants = {
  routeLink         : require('./routeLink'),
  mongoDB           : require('./mongoDB'),
  responseStatus    : require('./responseStatus'),
  consts            : require('./consts'),
  }
module.exports = constants
