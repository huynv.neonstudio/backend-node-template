module.exports=Object.freeze({
      MONGO_DATABASE_CONNECTION : "mongodb://admin:vtvcab321@10.104.25.36/?authSource=admin",
      MONGO_DBNAME :  "QLGD",
      MONGO_COLLECTTION_USERS : 'users',
      MONGO_COLLECTTION_AGENCIES : 'agencies',
      MONGO_COLLECTTION_POS_MACHINE : 'pos_machine',
      MONGO_COLLECTTION_BANKS : 'banks',
      MONGO_COLLECTTION_CARD_TYPES : 'card_types',
      MONGO_COLLECTTION_CARD_AGENCY_BANK : 'card_agency_banks',
      MONGO_COLLECTTION_TRADING_TIME_FRAMES : 'trading_time_frames',
      MONGO_COLLECTTION_ROLES : 'roles',
      MONGO_COLLECTTION_BILLS : 'bills',
      MONGO_COLLECTTION_CHECKOUT_DATE : 'checkout_dates',
      MONGO_COLLECTTION_PERMISSION_RESOURCE : 'permission_resources',
      MONGO_COLLECTTION_BILL_UPDATE_LOG: "bill_update_logs",
    })

