//SUCCESS 
module.exports={
SUCCESS  : {
    isSuccess : true,
    status : 200,
    message : "success !"
},
 CREATED :{
    isSuccess : true,
    status : 201,
    message : "data created !"
},
ACCEPTED :{
    isSuccess : true,
    status : 201,
    message : "accepted !"
},
//FAIL CLIENT 
BAD_REQUEST:{
    isSuccess : false,
    status : 400,
    message : "Bad Request !"
},
UNAUTHORIZED : {
    isSuccess : false,
    status : 401,
    message : "Unauthorized !"
},
 FORBIDDEN : {
    isSuccess : false,
    status : 403,
    message : "Forbidden !"
},
NOT_FOUND : {
    isSuccess : false,
    status : 404,
    message : "Resource not found !"
},
REQUEST_TIMEOUT :{
    isSuccess : false,
    status : 408,
    message : "Request timeout !"
},
REQUEST_CONFLICT:{
    isSuccess : false,
    status : 409,
    message : "Request conflict !"
},
// ERROR SERVER 
INTERNAL_SERVER_ERROR : {
    isSuccess : false,
    status : 500,
    message : "Internal Server Error !"
},
NOT_IMPLEMENTED : {
    isSuccess : false,
    status : 501,
    message : "Not Implemented !"
},
BAD_GATEWAY : {
    isSuccess : false,
    status : 502,
    message : "Bad gateway !"
}

}
