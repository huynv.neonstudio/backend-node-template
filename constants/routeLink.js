module.exports = Object.freeze({
    API                 : '/api',
    API_USER            : '/user',
    API_USER_LIST           : '/listUser',
    API_LOGIN           : '/login',
    API_USER_CREATE     : '/createUser',
    API_USER_UPDATE     : '/updateUser',
    API_GET_USER_INFO   : '/getUserInfo',
    API_REFRESH_TOKEN   : '/refreshToken',
    API_PRODUCT         : '/products',
    API_BILL            : '/bills',
    API_BILL_LIST            : '/listBill',
    API_VIEW_BILL_INFO            : '/viewBillInfo',
    API_UTILS           : '/utils',
    API_PRODUCT_LIST    : '/list',
    
    API_PRODUCT_VIEW    : '/viewProduct',
    API_PRODUCT_EDIT    : '/editProduct',
    API_BILL_CREATE    : '/createBill',
    API_BILL_ACCOUNT_CONFIRM   : '/accountantConfirm',
    API_BILL_UPDATE   : '/updateBill',
    API_BILL_MAKE_ERROR_FLAG   : '/makeErorrFlag',
    API_IMAGE_UPLOAD    : '/imageUpload',
    API_VALIDATE_TOKEN  : '/validateToken',

    API_BANK   : '/banks',
    API_BANK_CREATE    : '/createBank',
    API_BANK_UPDATE    : '/updateBank',
    API_BANK_LIST    : '/listBank',

    API_CARD_TYPE   : '/card_types',
    API_CARD_TYPE_CREATE    : '/createCardType',
    API_CARD_TYPE_UPDATE    : '/updateCardType',
    API_CARD_TYPE_LIST    : '/listCardType',

    API_CARD_AGENCY_BANK   : '/card_agency_banks',
    API_CARD_AGENCY_BANK_MAPPING_CREATE  : '/createMapping',
    API_CARD_AGENCY_BANK_MAPPING_UPDATE    : '/updateMapping',
    API_CARD_AGENCY_BANK_MAPPING_LIST    : '/listMapping',

    API_AGENCY   : '/agencies',
    API_AGENCY_CREATE    : '/createAgency',
    API_AGENCY_UPDATE    : '/updateAgency',
    API_AGENCY_LIST    : '/listAgency',

    API_POS_MACHINE   : '/pos_machine',
    API_POS_MACHINE_CREATE    : '/createPosMachine',
    API_POS_MACHINE_UPDATE    : '/updatePosMachine',
    API_POS_MACHINE_LIST    : '/listPosMachine',
    API_POS_LIST_BY_USER    : '/listPosByUser',


    API_TRADING_TIME_FRAME   : '/trading_time_frames',
    API_TRADING_TIME_FRAME_CREATE    : '/createTradingTimeFrame',
    API_TRADING_TIME_FRAME_UPDATE    : '/updateTradingTimeFrame',
    API_TRADING_TIME_FRAME_LIST    : '/listTradingTimeFrame',

    API_ROLE   : '/roles',
    API_ROLE_LIST    : '/listRole',


    API_CHECKOUT_DATE   : '/checkout_dates',
    API_API_CHECKOUT_DATE_CREATE   : '/createCheckoutDate',
    API_API_CHECKOUT_DATE_LIST   : '/listCheckoutDate',
    API_API_CHECKOUT_DATE_UPDATE   : '/updateCheckoutDate',



    API_REPORT   : '/reports',
    API_REPORT_BILL    : '/reportBill',
})