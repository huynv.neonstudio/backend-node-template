module.exports = Object.freeze({
    TIME_JWT_TOKEN_EXPIRE : '1d',
    TIME_JWT_REFRESH_TOKEN_EXPIRE : '7d',
    MAX_SIZE_UPLOAD : 5,
    EXPIRE_OTP : 5, //phút
    ERROR_TOKEN_EXPIRED : "error token expired",
    TOKEN_VALIDED : "token valided",
    EXPIRE_OTP : 5, //phút
    USER_DATA_DIDNT_EXISTED : "user data didn't exist",
    TIME_ELASPED : 10, // giờ,
    SERVER_LOCATION_FOR_STORAGE : './assets/fileUploads/original_images/',
    SERVER_LOCATION_FOR_STORAGE_THUMBNAIL : './assets/fileUploads/thumbnail_images/',
    URL_GENERATE_FOR_FILE_SHARE : 'http://10.104.31.38:3001/assets/fileUploads/original_images/',
    URL_GENERATE_FOR_FILE_SHARE_THUMBNAIL : 'http://10.104.31.38:3001/assets/fileUploads/thumbnail_images/',
    ERROR_APP_UPLOAD_FILE_NOT_ALLOW : "File not allow ",
})