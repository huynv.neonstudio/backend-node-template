
const MongoDatabase = require('./MongoDatabase')
const { mongoDB, responseStatus } = require('../../constants')

const objectID = require('mongodb').ObjectID
const { ObjectId } = require('bson')
const Functions = require('../../utils/Functions')

module.exports = {
    count : async(body)=>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CHECKOUT_DATE)
            let query = {}
            query._id = {}
            if(body.fromDate)
            {
                query._id["$gte"] = Functions.objectIdWithTimestamp(new Date(body.fromDate))
            }
            if(body.toDate)
            {
                query._id["$lte"] = Functions.objectIdWithTimestamp(new Date(body.toDate).addDays(1))
            }
            if(body.userid)
            {
                query.userid = userid
            }

            console.log(JSON.stringify(query))
            let result = await collection.find(query).count()
           
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    },
    find : async (body) =>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            const resultsPerPage = 10;
            let page = body.page >= 1 ? body.page : 1;
            page = page - 1
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CHECKOUT_DATE)
            let query = {}
            query._id = {}
            if(body.fromDate)
            {
                query._id["$gte"] = Functions.objectIdWithTimestamp(new Date(body.fromDate))
            }
            if(body.toDate)
            {
                query._id["$lte"] = Functions.objectIdWithTimestamp(new Date(body.toDate).addDays(1))
            }
            if(body.userid)
            {
                query.userid = userid
            }

            let result = await collection.find(query)
            .sort({ '_id': "asc" })
            .limit(resultsPerPage)
            .skip(resultsPerPage * page)
            .toArray()        
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    },
    insertOne: async (agency) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            agency.deleted = false
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CHECKOUT_DATE)
            let result = await collection.insertOne(agency)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findAll: async () => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CHECKOUT_DATE)
            let query = {deleted: false}
            let result = await collection.find(query).toArray()
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findOne: async (id) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CHECKOUT_DATE)
            let query =  { _id: objectID(id) }
            let result = await collection.findOne(query)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    updateOne : async (_id, data)=>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            if (data._id) delete data._id
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CHECKOUT_DATE)
            let result = await collection.updateOne(
                { _id: objectID(_id) },
                {
                    $set: data
                })
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    }
}