
const MongoDatabase = require('./MongoDatabase')
const { mongoDB, responseStatus } = require('../../constants')

const objectID = require('mongodb').ObjectID

module.exports = {
    find : async (body) =>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            // const resultsPerPage = 10;
            // let page = body.page >= 1 ? body.page : 1;
            // page = page - 1
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_USERS)
            let query = {}
            if(body.search){
                query =  { $or : [{ fullname: body.search },{ username: body.search }, {phone  :  body.search }, {CMND :body.search } ],
            
            }
            }else{
                query = {}
            }
            let result = await collection.find(query).project({_id:1,username:1,phone:1,CMND:1,roleId:1,pos_machines:1,fullname:1,birthday:1,active : 1})
            .sort({ '_id': "asc" })
            // .limit(resultsPerPage)
            // .skip(resultsPerPage * page)
            .toArray()        
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    },
    insertOne: async (insertData) => {
        insertData.deleted = false
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_USERS)
            let result = await collection.insertOne(insertData)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findByUsername: async (username) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_USERS)
            let query = {
                username: username,
            }
            let result = await collection.findOne(query)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    updateUser: async (_id, data) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            delete data._id;
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_USERS)
            let result = await collection.updateOne(
                { _id: objectID(_id) },
                {
                    $set: data
                })
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findById : async (_id , selectField={}) =>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_USERS)

            let result = await collection.findOne(
                { _id: objectID(_id)}, { projection: selectField} 
            )
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    },
    findUserByToken: async (token) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_USERS)

            let result = await collection.findOne(
                { token: token }
            )
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findUserByRefreshToken: async (refreshToken) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_USERS)

            let result = await collection.findOne(
                { refreshToken: refreshToken }
            )
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
}