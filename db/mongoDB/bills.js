
const MongoDatabase = require('./MongoDatabase')
const { mongoDB, responseStatus } = require('../../constants')

const objectID = require('mongodb').ObjectID
Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
  };
module.exports = {
    findReport : async (body) =>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            const resultsPerPage = 30;
            let page = body.page >= 1 ? body.page : 1;
            page = page - 1
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILLS)
            let query = {}
            if(body.status){
                query =  {status :body.status }
            }
            query.dateTransaction = {}
            if(body.userCreated)
            {
                query.userId = body.userCreated
            }
            if(body.fromDate)
            {
                query.dateTransaction["$gte"] = new Date(body.fromDate).toISOString()
            }
            if(body.toDate)
            {
                query.dateTransaction["$lte"] = new Date(body.toDate).addDays(1).toISOString()
            }
            if(body.errorFlag)
            {
                query.errorFlag = body.errorFlag;
            }
            if(body.posId)
            {
                query.posId= body.posId
            }
            if(body.bankId)
            {
                query.bankId= body.bankId
            }
            if(body.cardTypeId)
            {
                query.cardTypeId= body.cardTypeId
            }
            console.log(JSON.stringify(query))
            let result = await collection.find(query)
            .sort({ '_id': "desc" })
            .limit(resultsPerPage)
            .skip(resultsPerPage * page)
            .toArray()        
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    },
        count : async(body)=>{
            let db = await MongoDatabase.onConnect()
            if (!db)
                return responseStatus.INTERNAL_SERVER_ERROR
            else {
                const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILLS)
                let query = {}
                if(body.status){
                    query =  {status :body.status }
                }
                query.dateTransaction = {}
                if(body.userCreated)
                {
                    query.userId = body.userCreated
                }
                if(body.fromDate)
                {
                    query.dateTransaction["$gte"] = new Date(body.fromDate).toISOString()
                }
                if(body.toDate)
                {
                    query.dateTransaction["$lte"] = new Date(body.toDate).addDays(1).toISOString()
                }
                if(body.errorFlag)
                {
                    query.errorFlag = body.errorFlag;
                }
                if(body.pos_machineIds)
                {
                    query.posId= {
                                "$in": body.pos_machineIds
                            
                    }
                }
                console.log(JSON.stringify(query))
                let result = await collection.find(query).count()
               
                if (result)
                    return Object.assign({}, responseStatus.SUCCESS, { data: result })
                else
                    return responseStatus.NOT_FOUND
            } 
        },
        find : async (body) =>{
            let db = await MongoDatabase.onConnect()
            if (!db)
                return responseStatus.INTERNAL_SERVER_ERROR
            else {
                const resultsPerPage = 20;
                let page = body.page >= 1 ? body.page : 1;
                page = page - 1
                const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILLS)
                let query = {}
                if(body.status){
                    query =  {status :body.status }
                }
                query.dateTransaction = {}
                if(body.userCreated)
                {
                    query.userId = body.userCreated
                }
                if(body.fromDate)
                {
                    query.dateTransaction["$gte"] = new Date(body.fromDate).toISOString()
                }
                if(body.toDate)
                {
                    query.dateTransaction["$lte"] = new Date(body.toDate).addDays(1).toISOString()
                }
                if(body.errorFlag)
                {
                    query.errorFlag = body.errorFlag;
                }
                if(body.pos_machineIds)
                {
                    query.posId= {
                                "$in": body.pos_machineIds
                            
                    }
                }
                console.log(JSON.stringify(query))
                let result = await collection.find(query)
                .sort({ '_id': "desc" })
                .limit(resultsPerPage)
                .skip(resultsPerPage * page)
                .toArray()        
                console.log(result)
                if (result)
                    return Object.assign({}, responseStatus.SUCCESS, { data: result })
                else
                    return responseStatus.NOT_FOUND
            } 
        },
    
    insertOne: async (bill) => {
        bill.deleted  = false
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILLS)
            let query = {}
            let result = await collection.insertOne(bill)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findAll: async () => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILLS)
            let query = {deleted : false}
            let result = await collection.find(query).toArray()
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findOne: async (id) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILLS)
            let query =  { _id: objectID(id), deleted:false }
            let result = await collection.findOne(query)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    updateOne : async (_id, data)=>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            if (data._id) delete data._id
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILLS)
            let result = await collection.updateOne(
                { _id: objectID(_id) },
                {
                    $set: data
                })
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    }
}