
const MongoDatabase = require('./MongoDatabase')
const { mongoDB, responseStatus } = require('../../constants')

const objectID = require('mongodb').ObjectID

module.exports = {
    find: async (body) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            // const resultsPerPage = 20;
            // let page = body.page >= 1 ? body.page : 1;
            // page = page - 1

            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CARD_AGENCY_BANK)
            let query = {deleted : false}
            if(body.bankId)
            {
                query.bankId = body.bankId
            }
            if(body.cardId)
            {
                query.cardId = body.cardId
            }
            if(body.agencyId)
            {
                query.agencyId = body.agencyId
            }
            let result = await collection.find(query)
            .sort({ '_id': "desc" })
            // .limit(resultsPerPage)
            // .skip(resultsPerPage * page)
            .toArray()     
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    insertOne: async (insertData) => {
        insertData.deleted = false
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CARD_AGENCY_BANK)
            let result = await collection.insertOne(insertData)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findAll: async (body) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CARD_AGENCY_BANK)
            let query = {deleted : false}
            if(body.bankId)
            {
                query.bankId = body.bankId
            }
            let result = await collection.find(query).toArray()
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findByQuery: async (body) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CARD_AGENCY_BANK)
            let query = {deleted : false}
            if(body.bankId)
            {
                query.bankId = body.bankId
            }
            if(body.cardId)
            {
                query.cardId = body.cardId
            }
            if(body.agencyId)
            {
                query.agencyId = body.agencyId
            }
            
            let result = await collection.findOne(query)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    findOne: async (id) => {
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CARD_AGENCY_BANK)
            let query =  { _id: objectID(id) }
            let result = await collection.findOne(query)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },
    updateOne : async (_id, data)=>{
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.INTERNAL_SERVER_ERROR
        else {
            if (data._id) delete data._id
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_CARD_AGENCY_BANK)
            let result = await collection.updateOne(
                { _id: objectID(_id) },
                {
                    $set: data
                })
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        } 
    }
}