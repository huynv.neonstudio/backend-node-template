

const mongo = require('mongodb')
const constants = require('../../constants')
let instance = null
var dbConnected = null

class MongoDatabase {
    constructor() {
        if (!instance) {
            instance = this
            return instance
        }
        return instance
    }
    onConnect () {
        return new Promise((resolve, reject) => {
            if (!dbConnected) {
                mongo.MongoClient.connect(constants.mongoDB.MONGO_DATABASE_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
                    if (!err) {
                        dbConnected = db.db(constants.mongoDB.MONGO_DBNAME)
                        console.error("connect success")
                        db.on('close', () => {
                            console.error('connection to mongoDB has closed, trying to connect on next session ...');
                            dbConnected = null;
                        });
                        resolve(dbConnected)
                    } else {
                        console.error("error connected")
                        resolve(null)
                    }
                })
            } else {
                resolve(dbConnected)
            }
        })
    }

}
module.exports = new MongoDatabase()