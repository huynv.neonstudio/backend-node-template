
const MongoDatabase = require('./MongoDatabase')
const { mongoDB, responseStatus } = require('../../constants')

const objectID = require('mongodb').ObjectID

module.exports = {
    insertOne: async (insertData) => {
        insertData.deleted = false
        let db = await MongoDatabase.onConnect()
        if (!db)
            return responseStatus.NOT_FOUND
        else {
            const collection = db.collection(mongoDB.MONGO_COLLECTTION_BILL_UPDATE_LOG)
            let result = await collection.insertOne(insertData)
            console.log(result)
            if (result)
                return Object.assign({}, responseStatus.SUCCESS, { data: result })
            else
                return responseStatus.NOT_FOUND
        }
    },

}